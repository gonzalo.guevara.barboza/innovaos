# InnovaOS

Creación de distribución de paquetes basada en Ubuntu para el Proyecto Innovaciones Educativas del MEP, Costa Rica


## Instalación de Cubic
Se utiliza el paquete "cubic", por lo tanto es necesario instalarlo antes de inicial el proceso


```[bash]
sudo apt-add-repository -y ppa:cubic-wizard/release </br>
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 6494C6D6997C215E
sudo apt update
sudo apt -y install cubic
```


## Elaboración de la distribución de paquetes

Primero se escoge la ruta del folder donde se quiere guardar el proyecto

![Texto alternativo]( ReadmeFiles/Cubic-001.png "Título alternativo")

Luego se llenan los datos del proyecto

![Texto alternativo]( ReadmeFiles/Cubic-002.png "Título alternativo")
